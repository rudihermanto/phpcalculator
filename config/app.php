<?php

return [
    'providers' => [
        \Jakmall\Recruitment\Calculator\History\CommandHistoryServiceProvider::class => [
            \Jakmall\Recruitment\Calculator\Repository\FileDriver::class,
            \Jakmall\Recruitment\Calculator\Repository\DatabaseDriver::class
        ],
    ],
];
