<?php

namespace Jakmall\Recruitment\Calculator\History;

use Illuminate\Contracts\Container\Container;
use Jakmall\Recruitment\Calculator\Container\ContainerServiceProviderInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\FileCommandHistory;

class CommandHistoryServiceProvider implements ContainerServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $container, array $args): void
    {
        $container->bind(
            CommandHistoryManagerInterface::class,
            function () use ($container, $args) {
                //todo: register implementation
                $drivers = collect($args)
                    ->map(
                        function ($driver) use ($container) {
                            return $container->make($driver);
                        }
                    )
                    ->all();
                return new CommandHistoryManager($drivers);
            }
        );
    }
}
