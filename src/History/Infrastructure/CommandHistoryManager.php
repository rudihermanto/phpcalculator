<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

use Jakmall\Recruitment\Calculator\Repository\DatabaseDriver;
use Jakmall\Recruitment\Calculator\Repository\FileDriver;

class CommandHistoryManager implements CommandHistoryManagerInterface
{
    private $available_drivers;
    private $primary_drivers;

    public function __construct(array $available_drivers)
    {
        $this->available_drivers = $available_drivers;
        // $this->primary_drivers = new DatabaseDriver();
        
        // Temporary set FileDriver as default implementation
        // until I figure it out how to interact with database
        $this->primary_drivers = new FileDriver();
    }

    public function setPrimaryDriver($driver)
    {
        $this->primary_drivers = $driver;
    }

    public function find(array $commands)
    {
        return $this->primary_drivers->select($commands);
    }

    public function findAll(): array
    {
        return $this->primary_drivers->select();
    }

    public function log($command): bool
    {
        foreach ($this->available_drivers as $driver)
        {
            $driver->insert($command);
        }
        return true;
    }

    public function clearAll(): bool
    {
        foreach ($this->available_drivers as $driver)
        {
            $driver->deleteAll();
        }
        return true;
    }
}