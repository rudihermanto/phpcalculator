<?php

namespace Jakmall\Recruitment\Calculator\Repository;

interface HistoryInterface 
{
    public function select(array $commands = []): array;
    public function insert($command): bool;
    public function deleteAll(): bool;
}