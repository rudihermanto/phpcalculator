<?php

namespace Jakmall\Recruitment\Calculator\Repository;

use Exception;

class FileDriver implements HistoryInterface
{

    public function __construct()
    {
        if (!is_file($this->getFileName()) or !file_get_contents($this->getFileName())) {
            $this->initFile();
        }
    }

    public function select(array $commands = []): array
    {
        $raw_data = file_get_contents($this->getFileName());
        $history = json_decode($raw_data, true);
        if (count($commands) == 0) {
            return $history;
        }
        $result = array();
        foreach ($history as $row) {
            if (in_array($row['command'], $commands)) {
                array_push($result, $row);
            }
        }
        return $result;
    }

    public function insert($command): bool
    {
        $json_data = $this->select();
        array_push($json_data, $command);
        file_put_contents($this->getFileName(), json_encode($json_data));
        
        return true;
    }

    public function deleteAll(): bool
    {
        try {
            $this->initFile();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    private function getFileName() 
    {
        return 'history.json';
    }

    private function initFile() {
        $file = fopen($this->getFileName(), 'w');
        fwrite($file, "[]");
        fclose($file);
    }
}