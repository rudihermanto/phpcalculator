<?php

namespace Jakmall\Recruitment\Calculator\Commands;

class PowerCommand extends BaseCalculatorCommand
{
    protected function generateSignature(): string
    {
        return sprintf(
            '%s {base : The base number} {exp : The exponent number}',
            $this->getCommandVerb()
        );
    }

    protected function generateDescription(): string
    {
        return 'Exponent all given Numbers';
    }

    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    protected function getInput(): array
    {
        return [$this->argument('base'), $this->argument('exp')];
    }

    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return pow($number1, $number2);
    }

}
