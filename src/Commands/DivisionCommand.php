<?php

namespace Jakmall\Recruitment\Calculator\Commands;

class DivisionCommand extends BaseCalculatorCommand
{
    protected function generateSignature(): string
    {
        return sprintf(
            '%s {numbers* : The numbers to be %s}',
            $this->getCommandVerb(),
            $this->getCommandPassiveVerb()
        );
    }

    protected function generateDescription(): string
    {
        return sprintf('%s all given Numbers', ucfirst($this->getCommandVerb()));
    }

    protected function getCommandVerb(): string
    {
        return 'divide';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'divided';
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function getOperator(): string
    {
        return '/';
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 / $number2;
    }
}
