<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

abstract class BaseCalculatorCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var array
     */
    private $numbers;

    private $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
        $this->signature = $this->generateSignature();
        $this->description = $this->generateDescription();
        parent::__construct();
    }

    abstract protected function generateSignature(): string;
    abstract protected function generateDescription(): string;
    abstract protected function getCommandVerb(): string;
    abstract protected function getInput(): array;
    /**
     * @param float|int
     * @return float|int 
     */
    abstract protected function calculate($number1, $number2);

    protected function generateCalculationDescription(): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $this->numbers);
    }
    
    public function handle(): void
    {
        $this->numbers = $this->getInput();
        $description = $this->generateCalculationDescription();
        $result = $this->calculateAll($this->numbers);

        $this->comment($this->generateFullResult($description, $result));
        $this->logHistory($description, $result);
    }


    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    protected function logHistory($description, $result)
    {
        $command = [
            'command' => $this->getCommandVerb(),
            'description' => $description,
            'result' => $result,
            'output' => $this->generateFullResult($description, $result),
            'timestamp' => date("Y-m-d H:i:s")
        ];
        $this->history->log($command);
    }

    protected function generateFullResult($description, $result): string
    {
        return sprintf('%s = %s', $description, $result);
    }
}
