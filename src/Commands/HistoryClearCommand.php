<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    private $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
        $this->signature = $this->generateSignature();
        $this->description = $this->generateDescription();
        parent::__construct();
    }

    protected function generateSignature(): string
    {
        return $this->getCommandVerb();
    }

    protected function generateDescription(): string
    {
        return 'Clear saved history';
    }

    protected function getCommandVerb(): string
    {
        return 'history:clear';
    }

    public function handle(): void
    {
        if ($this->history->clearAll()) {
            $this->info("History Cleared!");
        } else {
            $this->info("Something went wrong!");
        }
    }
}
