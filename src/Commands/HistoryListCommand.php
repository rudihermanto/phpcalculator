<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\Repository\DatabaseDriver;
use Jakmall\Recruitment\Calculator\Repository\FileDriver;

class HistoryListCommand extends Command
{
    private $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
        $this->signature = $this->generateSignature();
        $this->description = $this->generateDescription();
        parent::__construct();
    }

    protected function generateSignature(): string
    {
        return sprintf(
            '%s {--D|driver=database : Driver for storage connection}{commands?* : Filter the history by commands}',
            $this->getCommandVerb()
        );
    }

    protected function generateDescription(): string
    {
        return 'Show calculator history';
    }

    protected function getCommandVerb(): string
    {
        return 'history:list';
    }

    public function handle(): void
    {
        $driver = $this->option('driver');
        $driver = $driver == 'database'? new DatabaseDriver() : new FileDriver();
        $this->history->setPrimaryDriver($driver);
        
        $filter = $this->argument('commands');
        $history = array();

        if (count($filter) == 0) {
            $history = $this->history->findAll();
        } else {
            $history = $this->history->find($filter);            
        }
        $this->displayHistory($history);
    }

    private function displayHistory($history)
    {
        if (count($history) == 0) {
            $this->info("History is empty.");
        } else {
            $this->table(
                $this->generateDisplayHeader($history),
                $this->generateDisplayBody($history)
            );
        }
    }

    private function generateDisplayHeader($history) {
        $headers = array("no");
        $headers = array_merge($headers, array_keys($history[0]));
        return array_map('ucfirst', $headers);
    }

    private function generateDisplayBody($history) {
        return collect($history)
            ->map(
                function ($row, $idx) {
                    array_unshift($row, $idx + 1);
                    return $row;
                }
            );
    }
}
